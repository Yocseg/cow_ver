#!/bin/bash

docker rm -f cowsay-ver
docker build -t cowsay_ver:1.0 .
docker run -d -p 4000:8080 --network versioned-cowsay_default --name cowsay-ver cowsay_ver:1.0
